# Handwriting to Latex
This respository contains code to convert a image of neatly printed handwrititen text and equations to LaTeX.

accuracy_data: Directory for storing figures of the model's accuracy

code: Directory containing code for the machine learning training

data: Contains the dataset of handwritten images for training the machine learning model

dataset_generation_code: Scripts used to generate the machine learning dataset
    
models: Directory to store the machine learning models  
- rand_c63_2_conv_2_pool_1_drop_b100_ep_20: The model with highest overall accuracy of 90.66%.  
- new_b100_e10_c63_2_conv_1_pool_1_drop: Model which does best on our sample image. Has an accuracy of 90.18% 

Jacob_Graham_Senior_Poster.pdf: Png of the poster associated with this project

hello.png: Sample image to test program

imageProcess.py: This is main script which converts an image of handwrititen text or math equations into latex code. 
Given an image from the user this script will output a tex file aptly named output.tex

output.tex: file to store the LaTeX output

posterPic.png: Another sample image

runner.sh: script to run the model training file on the Ada compute cluster
