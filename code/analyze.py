from sklearn.metrics import plot_confusion_matrix
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import itertools

# adjusted code to plot a confusion matrix
def plot_confusion_matrix(cm,
                          target_names,
                          title='Confusion matrix',
                          cmap=None,
                          normalize=True,
                          savefile="../accuracy_data/Confusion_matrix.png"):
    """
    given a sklearn confusion matrix (cm), make a nice plot

    Arguments
    ---------
    cm:           confusion matrix from sklearn.metrics.confusion_matrix

    target_names: given classification classes such as [0, 1, 2]
                  the class names, for example: ['high', 'medium', 'low']

    title:        the text to display at the top of the matrix

    cmap:         the gradient of the values displayed from matplotlib.pyplot.cm
                  see http://matplotlib.org/examples/color/colormaps_reference.html
                  plt.get_cmap('jet') or plt.cm.Blues

    normalize:    If False, plot the raw numbers
                  If True, plot the proportions

    Usage
    -----
    plot_confusion_matrix(cm           = cm,                  # confusion matrix created by
                                                              # sklearn.metrics.confusion_matrix
                          normalize    = True,                # show proportions
                          target_names = y_labels_vals,       # list of names of the classes
                          title        = best_estimator_name) # title of graph

    Citiation
    ---------
    http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html

    """
    print("SUM", float(np.sum(cm)))
    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(16, 12))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]


    thresh = cm.max() / 1.5 if normalize else cm.max() / 2

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))
    plt.savefig(savefile)

def top_guess(cm, close, head, savefile="../accuracy_data/StackedAccuracies.png"):
    """
    top_guess takes in a confusion matrix of expected vs. acutal class calls and graphs that data in the form of a stacked bar graph, where the bottom bar is the
    most frequently called class, the next stacked bar is the next most frequently called class and so on. The classes are ordered by their bottom stacked bar from
    lowest to highest frequecies. The image of the graph is saved at the specified file path.

    parameters:
    cm: the confusion matrix of dimensions # of classes * # of classes. The columns are the predicted class and the rows are the actual class label.
    close: integer specifying the number of stacked bars to show for each class. To account for all the class calls this should be equal to the total number of classes
    head: the number of classes to display on the graph

    returns: None
    """
    # dictionary to go from predicted softmax output to the character class
    Keys = {0:'0', 1:'1', 2:'2', 3:'3', 4:'4', 5:'5', 6:'6', 7:'7', 8:'8', 9:'9', 10:'A', 11:'B', 12:'C', 13:'D', 14:'E', 15:'F', 16:'G', 17:'H', 18:'I', 19:'J', 20:'K', 21:'L', 22:'M', 23:'N', 24:'O', 25:'P', 26:'Q', 27:'R', 28:'S', 29:'T', 30:'U', 31:'V', 32:'W', 33:'X', 34:'Y', 35:'Z', 36:'a', 37:'b', 38:'d', 39:'e', 40:'f', 41:'g', 42:'h', 43:'n', 44:'q', 45:'r', 46:'t', 47:'-', 48:'+', 49:'(', 50:')', 51:'=', 52:'theta', 53:'times', 54:'infty', 55: 'Sigma', 56: 'int', 57: 'times', 58: 'pm', 59: 'wedge', 60: 'approx', 61: 'forall', 62: 'in', 63: 'exists'}
    
    # array to store the call frequencies for each class
    close_calls = []
    temp_m = np.empty_like(cm)
    np.copyto(temp_m,  cm)
    for true in range(len(cm)):
        preds = temp_m[true]
        total_preds = np.sum(preds)
        # add a new class to the array
        print("KEY:", Keys[true], total_preds, "Values:", end=" ")
        close_calls.append([Keys[true], total_preds, []])


        # loop through the predicted calls for each class and add those to the close_calls array
        for call in range(close):
            max_pred = np.argmax(preds)
            close_char = Keys[max_pred]
            close_char_acc = (preds[max_pred]/total_preds)*100
            close_calls[true][2].append([close_char, close_char_acc])
            if close_char_acc > 0:
                print(f"{close_char}: {close_char_acc}", end=" ")
            preds[max_pred] = 0
        print()

    
    print("########################\n")

    # function to sort the classes by the highest call frequency
    def sort_call_acc(key_call):
        accs = [pair[1] for pair in key_call[2]]
        return accs[0]

    sorted_calls = sorted(close_calls, key = sort_call_acc)

    # extract the data for graphing
    calls_keys = []
    calls_accs = []
    ticks = []
    
    for key in sorted_calls:
        ticks.append(key[0])
        calls_keys.append(np.array([pair[0] for pair in key[2]]))
        calls_accs.append(np.array([pair[1] for pair in key[2]]))

    calls_keys = np.asarray(calls_keys)
    calls_accs = np.asarray(calls_accs)

    figure(figsize=(24, 6))

    # plts holds each layer of bars
    plts = []
    # datasets holds the frequency data for each class; updated for every layer of bars
    datasets = []
    ind = np.arange(head)
    width = .75
    
    for i in range(close):
        # get the position for the bottom of each bar
        bottom = np.asarray(datasets).sum(axis=0)
        if i == 0:
            bottom = np.zeros(head)

        datasets.append(np.array(calls_accs[:, i][:head]))
        
        colors = ['lightskyblue', 'violet', 'orange', 'aquamarine']

        # add the new layer of bars to the graph
        if i < len(colors):
            plts.append(plt.bar(ind, datasets[i], width, bottom=bottom, color = colors[i]))
        else:
            plts.append(plt.bar(ind, datasets[i], width, bottom=bottom))
        
        # label each bar the is greater than 3% frequency
        for j in range(head):
            data = datasets[i]
            if data[j] > 3:
                if len(calls_keys[j,i]) > 2:
                    plt.text(ind[j] - width/4, bottom[j] + data[j]/2 - 1, calls_keys[j,i], rotation='vertical')
                else:
                    plt.text(ind[j] - width/4, bottom[j] + data[j]/2 - 1, calls_keys[j,i])

    # add labels
    plt.ylabel('% Accuracy')
    plt.xlabel('Character')
    plt.title('Sorted Accuracies')
    plt.xticks(ind, ticks[:head],rotation=30)
    plt.yticks(np.arange(0, 100, 10))
    # save the figure
    plt.savefig(savefile)
    print("SAVED FIG:", savefile)

if __name__ == "__main__":
    num_classes = 63
    matrix = np.load("../accuracy_data/class63_matrix.npy")
    top_guess(matrix, num_classes, num_classes)
    print(matrix)
    keys = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','d','e','f','g','h','n','q','r','t', '-', '+', '(', ')', '=', 'theta', 'times', 'infty', 'Sigma', 'int', 'pm','wedge', 'approx', 'forall', 'in', 'exists']

    plot_confusion_matrix(matrix, keys, normalize=False)