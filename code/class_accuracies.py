import tensorflow as tf
from tensorflow import keras
import numpy as np
import os
from PIL import Image
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from analyze import top_guess, plot_confusion_matrix

"""
class_accuracies takes in an input model and a directory of validation images and generates the confusion matrix
of the model on the validation images. This matrix 
"""

def acc_matrix(model, test_dir, classes, keys, banned_keys, savefile=""):

    pred_matrix = np.zeros((classes, classes))
    count = 0
    for img_file in os.listdir(test_dir):
        key  = img_file.split(';')[0]
        
        if not key in banned_keys:

            img = Image.open(test_dir + '/' + img_file)
            img = img.convert("L")
            img = np.array(img).astype('float32') / 255
            img = img.reshape(1,28,28,1)

            
            pred = model(img).numpy()
            pred = np.argmax(pred)

            # print("KEY:", keys[key], "PRED:", pred)
            count += 1
            if count % 1000 == 0:
                print("FILES:", count)

            pred_matrix[keys[key]][pred] += 1
        

    for i in range(len(pred_matrix)):
        for j in range(len(pred_matrix)):
            print(pred_matrix[i][j], end=' ')
        print()

    if savefile:
        np.save(savefile, pred_matrix)
        print("SAVED MATRIX:", savefile)

    print(pred_matrix)
    return pred_matrix

modelPath = "../models/rand_c63_2_conv_2_pool_1_drop_b100_ep_20"
model = keras.models.load_model(modelPath)
test_dir = "../data/test"
classes = 63
keys = {'0': 0,'1': 1,'2': 2,'3': 3,'4': 4,'5': 5,'6': 6,'7': 7,'8': 8,'9': 9,'A': 10,'B': 11,'C': 12,'D': 13,'E': 14,'F': 15,'G': 16,'H': 17,'I': 18,'J': 19,'K': 20,'L': 21,'M': 22,'N': 23,'O': 24,'P': 25,'Q': 26,'R': 27,'S': 28,'T': 29,'U': 30,'V': 31,'W': 32,'X': 33,'Y': 34,'Z': 35,'a': 36,'b': 37,'c': 12,'d': 38,'e': 39,'f': 40,'g': 41,'h': 42,'i': 18,'j': 19,'k': 20,'l': 21,'m': 22,'n': 43,'o': 24,'p': 25,'q': 44,'r': 45,'s': 28,'t': 46,'u': 30,'v': 31,'w': 32,'x': 33,'y': 34,'z': 35, '-': 47, '+': 48, '(': 49, ')': 50, '=': 51, 'theta': 52, 'times': 53, 'infty': 54, 'Sigma': 55, 'int': 56, 'pm': 57,'wedge': 58, 'approx': 59, 'forall': 60, 'in': 61, 'exists': 62}
banned_keys = ['.',',','{','}','[',']','!']

confusion_matrix = acc_matrix(model, test_dir, classes, keys, banned_keys)

savefile = '../accuracy_data/' + modelPath.split('/')[-1] + '_accs.png'
top_guess(confusion_matrix, classes, classes, savefile)

matrixpath = '../accuracy_data/' + modelPath.split('/')[-1] + '_matrix.png'
plot_confusion_matrix(confusion_matrix, list(keys.keys()), normalize=False)