import tensorflow as tf
from tensorflow import keras
import numpy as np
from random import shuffle
from PIL import Image

class Data_Generator(keras.utils.Sequence):
    """
    Data_Generator contains the batch generation and image randomization code which is used in the model.fit function for model training. 
    The generator takes in the path to the image directory, the array of image filenames, and the desired batch size and generates image batches of 
    the specified size from the image directory using the array of image filenames
    """
    def __init__(self, container_dir, image_filenames, batch_size) :
        self.container_dir = container_dir
        self.image_filenames = image_filenames
        self.batch_size = batch_size
    
    def __len__(self) :
        # calculate the total number of batches
        return int(np.floor(len(self.image_filenames) / self.batch_size))
  
    def __getitem__(self, idx) :
        # randomize the image order at the beginning of each epoch to improve training accuracy
        if idx == 0:
            shuffle(self.image_filenames)
        
        # dictionary to conver the characters to their corresponding softmax output
        keys = {'0': 0,'1': 1,'2': 2,'3': 3,'4': 4,'5': 5,'6': 6,'7': 7,'8': 8,'9': 9,'A': 10,'B': 11,'C': 12,'D': 13,'E': 14,'F': 15,'G': 16,'H': 17,'I': 18,'J': 19,'K': 20,'L': 21,'M': 22,'N': 23,'O': 24,'P': 25,'Q': 26,'R': 27,'S': 28,'T': 29,'U': 30,'V': 31,'W': 32,'X': 33,'Y': 34,'Z': 35,'a': 36,'b': 37,'c': 12,'d': 38,'e': 39,'f': 40,'g': 41,'h': 42,'i': 18,'j': 19,'k': 20,'l': 21,'m': 22,'n': 43,'o': 24,'p': 25,'q': 44,'r': 45,'s': 28,'t': 46,'u': 30,'v': 31,'w': 32,'x': 33,'y': 34,'z': 35, '-': 47, '+': 48, '(': 49, ')': 50, '=': 51, 'theta': 52, 'times': 53, 'infty': 54, 'Sigma': 55, 'int': 56, 'pm': 57,'wedge': 58, 'approx': 59, 'forall': 60, 'in': 61, 'exists': 62}

        imgs = []
        labels = []

        # load the image in the specified input format
        def open_img(img_filename, container_dir):
            img = Image.open(container_dir + '/' + img_filename)
            img = img.convert("L")
            img = np.array(img).astype('float32') / 255
            img = img.reshape(28,28,1)
            return img
        
        # construct the batch of images and labels
        for i in range(idx * self.batch_size, (idx + 1) * self.batch_size):
            img_name = self.image_filenames[i]
            img = open_img(img_name, self.container_dir)
            imgs.append(img)

            # get the label of the image
            key  = img_name.split(';')[0]
            labels.append(keys[key])

        # convert the images and labels to np arrays
        imgs = np.array(imgs)
        labels = np.array(labels)
        labels = keras.utils.to_categorical(np.array(labels), 63) 
        return imgs, labels


        


