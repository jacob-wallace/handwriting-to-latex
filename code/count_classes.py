import os

"""
count_classes.py counts the number of imames for each character class in the specified directory and prints the output as a dictionary.
"""

# directory to search for files in
img_dir = "../data/train"
count = {}

# don't count images from these classes
banned_keys = ['.',',','{','}','[',']','!']

# loop through the images and count the number of images for each character class
for i in os.listdir(img_dir):
    key = i.split(';')[0]
    if key in count:
        count[key] += 1
    elif key not in banned_keys:
        count[key] = 0

print(count)