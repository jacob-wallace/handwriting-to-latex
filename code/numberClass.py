import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.datasets import mnist
from tensorflow.keras.layers import Input, Flatten, Dense, Dropout, Lambda, Conv2D, MaxPooling2D
from matplotlib import pyplot as plt
from callback import Save_on_epoch

import random
import os
from data_gen import Data_Generator
"""
numberClass.py trains the machine learning model on the handwritten images. At the end of the training, the model saves an graph of the training loss and accuracy
to  "../models/curves/". The training parameters at the top of the file can be adjusted.
"""
# training parameters
batch_size = 100
num_classes = 63
epochs = 20
save_freq = 10

# input image dimensions
img_rows, img_cols = 28, 28

# don't train on images from these classes
banned_keys = ['.',',','{','}','[',']','!']


train_dir = "../data/train"
test_dir = "../data/test"

# name to save the model
modelName = f"rand_c63_2_conv_2_pool_1_drop"
model_path = "../models/" + modelName + f"_b{batch_size}"

# enable use of the GPUs
gpus = tf.config.experimental.list_physical_devices('GPU')
print(gpus)
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)

# callback to save the model at specified intervals
save_every_10 = Save_on_epoch(model_path, save_freq)

# the data, split between train and test sets
train_filenames = [f for f in os.listdir(train_dir) if not f.split(';')[0] in banned_keys]
test_filenames = [f for f in os.listdir(test_dir) if not f.split(';')[0] in banned_keys]

print("Train Length", len(train_filenames))
print("Test Length", len(test_filenames))

# batch generators
training_generator = Data_Generator(train_dir, train_filenames, batch_size)
testing_generator = Data_Generator(test_dir, test_filenames, batch_size)

### BASE STRUCTURE ###
# model = Sequential()
# model.add(Conv2D(32, kernel_size=(3, 3),
#                  activation='relu',
#                  input_shape=(img_rows, img_cols, 1)))
# model.add(Conv2D(64, (3, 3), activation='relu'))
# model.add(MaxPooling2D(pool_size=(2, 2)))
# model.add(Flatten())
# model.add(Dropout(0.5))
# model.add(Dense(128, activation='relu'))
# model.add(Dropout(0.5))
# model.add(Dense(num_classes, activation='softmax'))

model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=(img_rows, img_cols, 1)))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))

print(model.summary())

# compile the model
model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer="adam",
              metrics=['accuracy'])

# train the model
history = model.fit(training_generator,
          batch_size=batch_size,
          epochs=epochs,
          verbose=2,
          validation_data=testing_generator,
          callbacks=[save_every_10])
score = model.evaluate(testing_generator, verbose=2)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

print(modelName)

# graph the training accuracy and loss
plt.subplot(211)
plt.title("Loss")
plt.plot(history.history["loss"], color="r", label="train")
plt.plot(history.history["val_loss"], color="b", label="validation")
plt.legend(loc="best")
plt.subplot(212)
plt.title("Accuracy")
plt.plot(history.history["accuracy"], color="r", label="train")
plt.plot(history.history["val_accuracy"], color="b", label="validation")
plt.legend(loc="best")

# save the image of the training graph
plt.savefig(os.path.join("../models/curves/" + modelName + ".png"))
print("../models/curves/" + modelName + ".png")



