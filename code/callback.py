import tensorflow as tf
from tensorflow import keras
import random

class Save_on_epoch(keras.callbacks.Callback):
    """
    Callback to save a model every specified number of epochs. The model is saved with the base modelName followed by the current number of epochs

    filepath: the base path (directory + base level model name) to save the model with
    freq: the desired frequncy that the model will be saved ex. if freq=10, the model will be saved every 10 epochs
    """
    def __init__(self, filepath, freq):
        self.filepath = filepath
        self.freq = freq

    def on_epoch_end(self, epoch, logs=None):
        # if model save frequncy has come
        if (epoch+1) % self.freq == 0:
            path = self.filepath + f"_ep_{epoch+1}"
            self.model.save(path)
            print(f"SAVED MODEL: {path}")