import os
import shutil

container_dir = "/home/jwallace/handwriting-to-latex/data/filtered_imgs"
test_dir = "/home/jwallace/handwriting-to-latex/data/crohme_test"
train_dir = "/home/jwallace/handwriting-to-latex/data/crohme_train"

for key_dir in os.listdir(container_dir):
    img_files = os.listdir(container_dir + '/' + key_dir)
    test_split = img_files[:len(img_files)//7]
    train_split = img_files[len(img_files)//7:]
    
    print("Len Test:", len(test_split))
    print()

    for file in test_split:
        old_name = container_dir + '/' + key_dir + '/' + file
        new_name = test_dir + '/' + key_dir + ';' + file
        os.rename(old_name, new_name)

        print("OLD:", old_name, "NEW", new_name)

    print("Len Train:", len(train_split))
    print()

    for file in train_split:
        old_name = container_dir + '/' + key_dir + '/' + file
        new_name = train_dir + '/' + key_dir + ';' + file
        os.rename(old_name, new_name)

        print("OLD:", old_name, "NEW", new_name)


    
    


