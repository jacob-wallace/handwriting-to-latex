import cv2
import numpy as np

"""
Code to test various image modification to create output images matching the style of the EMNIST data
"""

# open the source image
img_path = "/Users/jacobwallace/Desktop/testDataFolder/extracted_images/infty/exp2456.jpg"
img = cv2.imread(img_path)

# flip any white backgrounds to black
ret, img = cv2.threshold(img, 120, 255, cv2.THRESH_BINARY_INV)
# add a border to the image
bordersize = 6
img = cv2.copyMakeBorder(
    img,
    top=6,
    bottom=8,
    left=3,
    right=5,
    borderType=cv2.BORDER_CONSTANT,
    value=[0, 0, 0]
)
# dilate the image
cv2.imwrite("bordered.png", img)
kernel = np.full((2, 2), 1, dtype = np.uint8)
img = cv2.dilate(img, kernel, iterations=3)

cv2.imwrite("dialated.png", img)

# resize the image
dsize = (28,28)
img = cv2.resize(img, dsize, interpolation = cv2.INTER_AREA)

cv2.imwrite("resized_area.png", img)