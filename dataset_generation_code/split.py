import os
import shutil

"""
Code to create the test/train split from an image directory
"""

# directory containing all the images
container_dir = "/Users/jacobwallace/Desktop/Middlebury 20-21/CSCI0701/project/data/hasy-imgs"
# destination directories for the testing and training split
test_dir = "/Users/jacobwallace/Desktop/Middlebury 20-21/CSCI0701/project/data/hasy_test"
train_dir = "/Users/jacobwallace/Desktop/Middlebury 20-21/CSCI0701/project/data/hasy_train"


img_files = os.listdir(container_dir)
classes = {}

# sort the images by class
for img_file in img_files:
    key = img_file.split(';')[0]
    if key in classes:
        classes[key].append(img_file)
    else:
        classes[key] = [img_file]

for key, value in classes.items():
    print("KEY", key, "VALUES", len(value))

# split each class of images to create a 1:6 test:train split
for key in classes:
    test_split = classes[key][:len(classes[key])//7]
    train_split = classes[key][len(classes[key])//7:]
    print("KEY:", key)
    print("Len Test:", len(test_split))

    for file in test_split:
        old_name = container_dir  + '/' + file
        new_name = test_dir + '/' + file
        os.rename(old_name, new_name)

        print("OLD:", old_name, "NEW", new_name)

    print("Len Train:", len(train_split))
    print()

    for file in train_split:
        old_name = container_dir + '/' + file
        new_name = train_dir + '/' + file
        os.rename(old_name, new_name)

        print("OLD:", old_name, "NEW", new_name)

print("TEST LEN", len(os.listdir(test_dir)))
print("TRAIN LEN", len(os.listdir(train_dir)))
    
    


