
# Core Library modules
import csv
import json
import os
import sys
import numpy as np
import pandas as pd
from math import log

# Third party modules
from PIL import Image, ImageDraw
import cv2

"""
Create hwrt-offline dataset from the point-sequence data from https://zenodo.org/record/50022. Program takes in
the csv file of point-sequence data and the corresponding labels csv file and generates png images saved to 
the specified directory

Code adapted from https://github.com/MartinThoma/HASY
"""

# desired keys
KEYS = ["\\Sigma", "\\int", "\\times", "\\pm", "\\wedge", "\\approx", "\\forall", "\\in", "\\exists", "\\infty"]

def load_csv(filepath):
    """Read a CSV file."""
    data = []
    with open(filepath, "rt") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=";")
        for row in reader:
            data.append(row)
    return data

def load_dataset(filepath):
    """Load a HWRT dataset."""
    data = load_csv(filepath)
    print("Done With Load")
    dicts = []
    for i, row in enumerate(data):
        row["data"] = normalize_data(json.loads(row["data"]), 42, 42)
        dicts.append(row)

    return dicts

def normalize_data(lines, width, height):
    """Normalize the data to be centered within a bounding box."""
    # Find the bounding dimensions of the character
    min_x, max_x = float("inf"), 0
    min_y, max_y = float("inf"), 0
    for line in lines:
        for p in line:
            min_x = min(min_x, p["x"])
            max_x = max(max_x, p["x"])
            min_y = min(min_y, p["y"])
            max_y = max(max_y, p["y"])
    dimensions = max(max_x - min_x, max_y - min_y)
    dimensions = max(42.0, float(dimensions))

    # translation to align the data within the bounding box
    x_test_trans = (52 - (max_x - min_x) / dimensions * width) / 2
    y_test_trans = (52 - (max_y - min_y) / dimensions * width) / 2
    for i, line in enumerate(lines):
        for j, p in enumerate(line):
            # normalize the image and center it
            lines[i][j]["x"] = (p["x"] - min_x) / dimensions * width + x_test_trans
            lines[i][j]["y"] = (p["y"] - min_y) / dimensions * height + y_test_trans
    return lines


def draw(target_path, lines):
    """Create an image for online data."""
    width, height = 56, 56
    im = Image.new("L", (width, height), 0)
    draw = ImageDraw.Draw(im)
    for line in lines:
        had_difference = False
        p = line[0]
        for p1, p2 in zip(line, line[1:]):
            if int(abs(p1["x"] - p2["x"])) + int(abs(p1["y"] - p2["y"])) > 0:
                had_difference = True
            draw.line((p1["x"], p1["y"], p2["x"], p2["y"]), fill=255, width=1)
        if not had_difference:
            r = 2
            x, y = p["x"], p["y"]
            draw.ellipse((x - r, y - r, -x + r, y + r), fill=255)
    
    # dilate and resize line drawings to closley match EMNIST data
    cv_im = np.array(im)
    kernel = np.full((2, 2), 1, dtype = np.uint8)
    cv_im = cv2.dilate(cv_im, kernel, iterations=4)
    dsize = (28,28)
    cv_im = cv2.resize(cv_im, dsize, interpolation = cv2.INTER_AREA)
    cv2.imwrite(target_path, cv_im)


def generate_dataset(data, symbols_dict, directory):
    """Generate a dataset."""
    if not os.path.exists(directory):
        os.makedirs(directory)
    else:
        print("Directory '%s' already exists. Please remove it.")
        sys.exit(-1)
    print(
        "Start generating 32x32 images for %i instances of %i symbols"
        % (len(data), len(symbols_dict))
    )
    labels = [("path", "symbol_id", "latex", "user_id")]
    format_str = "v2-{0:0%id}" % log(len(data), 10)

    count = 0
    for i, el in enumerate(data):
        if i % 1000 == 0:
            print("\t%i done" % i)
        key = symbols_dict[el["symbol_id"]]

        if key in KEYS:
            
            skey = key.replace("\\","")
            target_path = f"{directory}/{skey};{format_str.format(i)}.png"
            draw(target_path, lines=el["data"])
            labels.append(
                (target_path, el["symbol_id"], symbols_dict[el["symbol_id"]], el["user_id"])
            )
        count += 1

    with open("%s-labels.csv" % directory, "w") as f:
        a = csv.writer(f, delimiter=",")
        a.writerows(labels)

def main():
    """Generate the dataset."""
    # find the path to the data
    data_dir = "/Users/jacobwallace/Desktop/Middlebury 20-21/CSCI0701/project/2015-01-28-data"
    if not os.path.isdir(data_dir):
        print(
            f"Please download https://zenodo.org/record/50022 and extract "
            f"it to {data_dir}"
        )
        return
    print(data_dir + "/symbols.csv")
    # load the data
    data = load_dataset(data_dir + "/test-data.csv")
    symbols_df = load_csv(data_dir + "/symbols.csv")
    symbols = {}
    for row in symbols_df:
        symbols[row["symbol_id"]] = row["latex"]
    
    # print(symbols)

    # generate and save the images
    data = load_dataset(data_dir + "/test-data.csv")
    print("Start loading train data...")
    data = data + load_dataset(data_dir + "/train-data.csv")
    temp_dir = "/Users/jacobwallace/Desktop/Middlebury 20-21/CSCI0701/project/data/hasy-imgs"
    generate_dataset(data, symbols, directory=temp_dir)


if __name__ == "__main__":
    main()