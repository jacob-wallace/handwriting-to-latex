import os

"""
Count the images in an outer directory containing nested image directories
"""

img_directory = "/Users/jacobwallace/Desktop/Middlebury 20-21/CSCI0701/project/data/filtered_imgs"

total = 0
for img_folder in os.listdir(img_directory):
    if (not img_folder[0] == '.'):
        print(img_folder, end='\t')
        counter = 0
        for img in os.listdir(img_directory + '/' + img_folder):
            counter += 1
        print(counter)
        total += counter
print(total)