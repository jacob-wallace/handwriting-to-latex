import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import numpy as np # linear algebra
import cv2 as cv
import os


train_data_path = 'emnist-balanced-train.csv'
test_data_path = 'emnist-balanced-test.csv'

train_data = pd.read_csv(train_data_path, header=None)
test_data = pd.read_csv(test_data_path, header=None)
class_mapping = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabdefghnqrt'

def save_images(data):
    ROI_number= 0
    main_folder = "emnist_train_all"
    os.makedirs(main_folder)
    for i in range(len(data.values)):
        img_flip = np.transpose(data.values[i, 1:].reshape(28, 28), axes=[1,0]) # img_size * img_size arrays
        img = img_flip.astype(np.uint8)
        currentLetter = class_mapping[data.values[i, 0]]
        cv.imwrite(os.path.join(main_folder, currentLetter + 'ROI_{}.png'.format(ROI_number)), img)
        ROI_number += 1

save_images(train_data)  #switch to test data to get both data sets
