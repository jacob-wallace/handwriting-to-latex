import numpy as np
import cv2
import os
import shutil

"""
filter_imgs.py takes in a directory of images sourced from https://www.kaggle.com/xainano/handwrittenmathsymbols.
The duplicate images in the directory are removed using hashing techniques and the unquie images are transformed 
to closely match the form of the images in the EMNIST dataset.
"""

def dhash(image, hashSize=8):
    """
    dhash takes in an image array and outputs a unique integer hash of size hashsize
    """
	# adding a single column (width) so we can compute the horizontal gradient
	resized = cv2.resize(image, (hashSize + 1, hashSize))
	# compute the (relative) horizontal gradient between adjacent column pixels
	diff = resized[:, 1:] > resized[:, :-1]
	# convert the difference image to a hash and return it
	return sum([2 ** i for (i, v) in enumerate(diff.flatten()) if v])

def modify(img):
    """
    modify takes in an input image and modifies it to have similar characteristics to the EMNIST data.
    The background is converted to black and the character to white; a border is added to the image;
    the single pixel wide lines are dilated; and the image is resized to 28x28.
    """
    # flip the black and white in the image
    ret, img = cv2.threshold(img, 120, 255, cv2.THRESH_BINARY_INV)
    # add a border to the image to match that of the EMNIST data
    img = cv2.copyMakeBorder(
        img,
        top=6,
        bottom=8,
        left=3,
        right=5,
        borderType=cv2.BORDER_CONSTANT,
        value=[0, 0, 0]
    )
    # dilate the lines 
    kernel = np.full((2, 2), 1, dtype = np.uint8)
    img = cv2.dilate(img, kernel, iterations=3)
    # resize the image
    dsize = (28,28)
    img = cv2.resize(img, dsize, interpolation = cv2.INTER_AREA)
    return img

# array to store the unique hashes
filter_set = []

# source and destination directories
img_dir = "/Users/jacobwallace/Desktop/testDataFolder/extracted_images/times"
filtered_dir = "/Users/jacobwallace/Desktop/testDataFolder/filtered_imgs"

count = 0
duplicates = 0

# make a new folder to store the filtered images
key = img_dir.split('/')[-1]
folder = filtered_dir + '/' + key
print(folder)
os.mkdir(folder)

# remove the duplicate image and modify unique ones and add to the new folder
for img_path in os.listdir(img_dir):
    #print(img_path)
    count += 1
    img = cv2.imread(img_dir + '/' + img_path)
    h = dhash(img)
    
    if h not in filter_set:
        filter_set.append(h)
        save_path = folder + '/' + img_path
        print(save_path)
        img = modify(img)
        cv2.imwrite(save_path, img)
    else:
        duplicates += 1

print(len(filter_set))
print(duplicates)
print(count)