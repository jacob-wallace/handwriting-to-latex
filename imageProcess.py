import itertools

from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import cv2 as cv
import numpy as np
from autocorrect import Speller


def sortVal(val):
    """
    This function is used to help sort a list
    For the purposes of this program we are using this to sort images by y coordinates or x coordinates
    ie left to right
    """
    return val[1]


def finding_charactors_with_edge_detection(image):
    """
    This function takes in an image containing just a word and returns a list of the characters in the word.
    each image of a character is processed and reshaped so it is ready to be predicted on by the machine learning model.
    """
    original = image.copy()

    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray, (3, 3), 0)
    canny = cv.Canny(blur, 120, 255, 1)  # canny edge detection

    cnts = cv.findContours(canny, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]

    for c in cnts:
        cv.drawContours(canny, [c], 0, 255, -1)

    kernel = cv.getStructuringElement(cv.MORPH_RECT, ksize=(1, 6))
    dilated = cv.dilate(canny, kernel, iterations=4)


    for i in range(2): # remove small contours
        cnts = cv.findContours(canny, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        rect_areas = []
        for c in cnts:
            (x, y, w, h) = cv.boundingRect(c)
            rect_areas.append(w * h)
        avg_area = np.mean(rect_areas)
        for c in cnts:
            (x, y, w, h) = cv.boundingRect(c)
            cnt_area = w * h
            if cnt_area < 0.05 * avg_area:
                canny[y:y + h, x:x + w] = 0

    cnts = cv.findContours(dilated, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]

    numbersImgList = []
    for c in cnts:
        x, y, w, h = cv.boundingRect(c)
        if h > w:
            makeSquare = int((h - w) / 2)
            border = int(w * .15)

            ROI = original[y:y + h, x:x + w]
            ROI = cv.cvtColor(ROI, cv.COLOR_BGR2GRAY)
            ROI = cv.threshold(ROI, 0, 255,
                               cv.THRESH_BINARY_INV | cv.THRESH_OTSU)[1]
            kernel = np.ones((3, 3), np.uint8)
            ROI = cv.dilate(ROI, kernel, iterations=1)
            ROI = cv.erode(ROI, kernel, iterations=1)

            ROI = cv.copyMakeBorder(ROI, border, border, makeSquare + border, makeSquare + border, cv.BORDER_CONSTANT)
            numbersImgList.append((ROI, x))
        else:
            makeSquare = int((w - h) / 2)
            border = int(w * .15)

            ROI = original[y:y + h, x:x + w]
            ROI = cv.cvtColor(ROI, cv.COLOR_BGR2GRAY)
            ROI = cv.threshold(ROI, 0, 255,
                               cv.THRESH_BINARY_INV | cv.THRESH_OTSU)[1]
            kernel = np.ones((3, 3), np.uint8)
            ROI = cv.dilate(ROI, kernel, iterations=1)
            ROI = cv.erode(ROI, kernel, iterations=1)

            ROI = cv.copyMakeBorder(ROI, border, border, makeSquare + border, makeSquare + border, cv.BORDER_CONSTANT)
            numbersImgList.append((ROI, x))

    numbersImgList.sort(key=sortVal) # sort chars from left to right

    output = []
    for im in numbersImgList:
        img = cv.resize(im[0], (28, 28), interpolation=cv.INTER_AREA)  # resize
        # reshape into a single sample with 1 channel
        img = img.reshape(1, 28, 28, 1)
        # prepare pixel data
        img = img.astype('float32')
        img = img / 255.0

        output.append(img)
    return output


def find_words_with_edge_detection(image):
    """
    Given the staring image this fucntion determines the bounding box of each word found in the image.
    Each word is then sorted based off of which line it appears in and then each line is sorted from left to right.
    """
    image = cv.resize(image, (600, 800))
    original = image.copy()

    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray, (3, 3), 0)
    canny = cv.Canny(blur, 120, 255, 1)
    kernel = cv.getStructuringElement(cv.MORPH_RECT, (9, 3))
    dilate = cv.dilate(canny, kernel, iterations=4)
    kernel = cv.getStructuringElement(cv.MORPH_RECT, (9, 1))
    dilate = cv.dilate(dilate, kernel, iterations=1)

    avg_height = 0
    for i in range(2): # remove contours that are too small to be a word
        cnts = cv.findContours(dilate, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]

        rect_areas = []
        heights = []
        for c in cnts:
            (x, y, w, h) = cv.boundingRect(c)
            rect_areas.append(w * h)
            heights.append(h)
        avg_area = np.mean(rect_areas)
        avg_height = np.mean(heights)
        for c in cnts:
            (x, y, w, h) = cv.boundingRect(c)
            cnt_area = w * h
            if cnt_area < 0.1 * avg_area:
                dilate[y:y + h, x:x + w] = 0

    words = []
    for c in cnts:
        x, y, w, h = cv.boundingRect(c)
        cv.rectangle(image, (x, y), (x + w, y + h), (36, 255, 12), 2)
        ROI = original[y:y + h, x:x + w]
        words.append((ROI, y, x))
    words.sort(key=sortVal) # sort words by y value on the picture

    ordering = []
    while bool(words):  # while empty
        currentLine = words[0][1]  # current line height
        line = []
        remove = []
        for word in words:
            if word[1] < currentLine + (avg_height * .6):  # same line
                line.append((word[0], word[2]))
                remove.append(word)
        for word in remove:
            words.remove(word)
        line.sort(key=sortVal) # sort so each word is left to right
        finalLine = []
        for word in line:
            finalLine.append(word[0])
        ordering.append(finalLine) # remove the x coordinate

    return ordering


def isMath(value):
    """
    A function returning a boolean on whether character is a formula should be replaced
    """
    dict = {'0': True, '1': True, '2': True, '3': True, '4': True, '5': True, '6': True, '7': True, '8': True, '9': True, 'A': True, 'B': True, 'C': False, 'D': False,
     'E': False, 'F': False, 'G': True, 'H': False, 'I': False, 'J': False, 'K': True, 'L': False, 'M': False, 'N': True, 'O': False, 'P': False,
     'Q': False, 'R': False, 'S': False, 'T': False, 'U': False, 'V': False, 'W': False, 'X': True, 'Y': True, 'Z': False, 'a': True, 'b': True,
     'd': False, 'e': False, 'f': False, 'g': False, 'h': True, 'n': True, 'q': False, 'r': False, 't': False, '-': True, '+': True, '(': True,
     ')': True, '=': True, '\\theta': True, '\\times': True, '\\infty': True, '\\Sigma': True, '\\int': True, '\\pm': True, '\\wedge': True,
     '\\approx': True, '\\forall': True, '\\in': True, '\\exists': True} # if a char is mapped to true then it is considered math and won't be replaced in a formula

    key = dict[value]
    return key


def run_main():
    # load the image
    image = cv.imread("hello.png")
    paragraph = find_words_with_edge_detection(image)  # image processing to find each word

    my_dict = Keys = {0:'0', 1:'1', 2:'2', 3:'3', 4:'4', 5:'5', 6:'6', 7:'7', 8:'8', 9:'9', 10:'A', 11:'B', 12:'C',
                      13:'D', 14:'E', 15:'F', 16:'G', 17:'H', 18:'I', 19:'J', 20:'K', 21:'L', 22:'M', 23:'N', 24:'O',
                      25:'P', 26:'Q', 27:'R', 28:'S', 29:'T', 30:'U', 31:'V', 32:'W', 33:'X', 34:'Y', 35:'Z', 36:'a',
                      37:'b', 38:'d', 39:'e', 40:'f', 41:'g', 42:'h', 43:'n', 44:'q', 45:'r', 46:'t', 47:'-', 48:'+',
                      49:'(', 50:')', 51:'=', 52:'\\theta', 53:'\\times', 54:'\\infty', 55: '\\Sigma', 56: '\\int', 57: '\\pm',
                      57: '\\wedge', 59: '\\approx', 60: '\\forall', 61: '\\in', 62: '\\exists'}


    spell = Speller(lang='en')  # loading a dictionary to spell check / catch small errors in english words
    model = load_model('models/rand_c63_2_conv_2_pool_1_drop_b100_ep_20')  # loading the machine learning model

    para = ""
    outputList = my_dict.values()
    for line in paragraph:  # iterate through each line
        sentence = ""
        for img in line:  # for each word in a line
            mathCount = 0
            totalLength = 0
            img = finding_charactors_with_edge_detection(img)  # image processing to find each letter
            # predict the class
            word = []
            wordPredictor = []
            for im in img: # predict on each letter
                digit = model.predict_classes(im)
                predictions = model.predict_proba(im)
                predictions = sorted(zip(predictions[0], outputList), reverse=True)[:4]
                word.append(my_dict.get(digit[0]))
                wordPredictor.append(predictions)
                if not (digit[0] > 9 and digit[0] < 47):
                    mathCount = mathCount + 1
                totalLength = totalLength + 1
            mathPercentage = mathCount / totalLength

            stringWord = ""
            if mathPercentage >= .5:  # numbers and symbols
                for i in range(len(word)):
                    if not isMath(word[i]):
                        for p in wordPredictor[i]:
                            if isMath(p[1]):
                                word[i] = p[1]
                                break
                stringWord = ''.join([str(elem) for elem in word])
                stringWord = "$" + stringWord + "$"
            else:  # english word
                res = []
                indices = []
                corrections = []
                for i in range(len(word)):
                    if not word[i].isalpha():
                        want = [i[1] for i in wordPredictor[i][1:]]  # just char suggestion
                        corrections.append(want)
                        indices.append(i)
                res = list(itertools.product(*corrections))
                if bool(res[0]):
                    for guess in res:
                        fix = ""
                        count = 0
                        for i in range(len(word)):
                            if i in indices:
                                fix = fix + str(guess[count])
                                count = count + 1
                            else:
                                fix = fix + word[i]
                        if spell(fix.lower()) == fix.lower():
                            stringWord = fix.lower()
                            break
                        else:  # if no suggestion works just take the original guess
                            stringWord = ''.join([str(elem) for elem in word])



                else:
                    stringWord = ''.join([str(elem) for elem in word])
                    stringWord = spell(stringWord.lower())

            sentence = sentence + " " + stringWord

        para = para + "\n" + sentence

    print(para)
    filename = "output.tex"
    outfile = open(filename, 'w')

    outfile.writelines("\\documentclass[11pt]{article}\n")
    outfile.writelines("\\begin{document}")
    outfile.writelines(para)
    outfile.writelines('\n')
    outfile.writelines("\\end{document}")

    outfile.close()


# entry point, run the example
run_main()
